module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  // plugins: [
  //   [
  //     'module-resolver',
  //     {
  //       root: ['.'],
  //       extensions: ['.ios.js', '.android.js', '.js', '.ts', '.tsx', '.json'],
  //       alias: {
  //         "_components": "./src/components",
  //         "_components/*": "./src/components/*",
  //         "_assets":"./src/assets",
  //         "_assets/*":"./src/assets/*",
  //         "_atoms":"./src/components/atoms",
  //         "_atoms/*":"./src/components/atoms/*",
  //         "_svg":"./src/assets/svg",
  //         "_svg/*":"./src/assets/svg/*",
  //         "_config":"./src/config",
  //         "_config/*":"./src/config/*",
  //         "_scenes": "./src/scenes",
  //         "_scenes/*": "./src/scenes/*",
  //         "_navigation":"navigations",
  //         "_navigation/*":["navigations/*"]
  //       }
  //     }
  //   ]
  // ]
  plugins: [
    [
      'module-resolver',
      {
        root: ['./src'],
        extensions: ['.ios.js', '.android.js', '.js', '.ts', '.tsx', '.json'],
        alias: {
          "@components": "./src/components",
          "@navigations": "./src/navigations",
          "@config":"./src/config",
          "@scenes": "./src/scenes",
          "@services": "./src/services",  
          "@assets": "./src/assets",
          "@assets/*": "./src/assets/*",
          "@components/*": "./src/components/*",
          "@navigations/*": "./src/navigations/*",
          "@scenes/*": "./src/scenes/*",
          "@services/*": "./src/services/*",
          "@config/*":"./src/config/*"
        }
      }
    ]
  ]

};
