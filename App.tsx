import React from 'react';
import Navigator from './src/navigations';
import { LogBox, View } from 'react-native';
import { Provider } from 'react-redux';
import { store } from "@services/redux/store"
import SQLite from 'react-native-sqlite-storage'

const App = () => {
  LogBox.ignoreLogs(['Warning: ...'])
  LogBox.ignoreAllLogs();
  SQLite.enablePromise(true);
  return (
    <Provider store={store}>
      <Navigator />
    </Provider>
  );
};

export default App;
