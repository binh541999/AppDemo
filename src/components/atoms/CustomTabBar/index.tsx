import React from 'react';
import { Text, TouchableOpacity, View } from "react-native";
import Feather from 'react-native-vector-icons/Feather';
import AntDesign from 'react-native-vector-icons/AntDesign';
import FontAwesome from 'react-native-vector-icons/FontAwesome';


const CustomTabBar = ({ state, descriptors, navigation }) => {
    return (
        <View style={{ height: 60, flexDirection: 'row', borderTopWidth: 1, alignItems:'center' }}>
            {state.routes.map((route: any, index: any) => {
                const { options } = descriptors[route.key];
                const label =
                    options.tabBarLabel !== undefined
                        ? options.tabBarLabel
                        : options.title !== undefined
                            ? options.title
                            : route.name;

                const isFocused = state.index === index;

                let icon =  <Feather name='settings' size={30} color={isFocused ? '#005BFF' : '#7D8799'} />

                switch (index) {
                    case 0: {
                        icon = <AntDesign name='contacts' size={30} color={isFocused ? '#005BFF' : '#7D8799'} />
                        break
                    }
                    case 1: {
                        icon = <FontAwesome name='group' size={30} color={isFocused ? '#005BFF' : '#7D8799'} />
                        break
                    }                  
                    default: icon = <AntDesign name='contacts' size={30} />
                }

                const onPress = () => {
                    const event = navigation.emit({
                        type: 'tabPress',
                        target: route.key,
                        canPreventDefault: true,
                    });

                    if (!isFocused && !event.defaultPrevented) {
                        navigation.navigate(route.name);
                    }
                };

                const onLongPress = () => {
                    navigation.emit({
                        type: 'tabLongPress',
                        target: route.key,
                    });
                };

                return (
                    <View style={{ flex: 1 }} key={route.key}>
                        <TouchableOpacity
                            accessibilityRole="button"
                            accessibilityState={isFocused ? { selected: true } : {}}
                            accessibilityLabel={options.tabBarAccessibilityLabel}
                            testID={options.tabBarTestID}
                            onPress={onPress}
                            onLongPress={onLongPress}
                            style={{ justifyContent: 'center', alignItems: 'center', flexDirection: 'column' }}
                        >
                            {icon}
                            <Text style={{ fontSize: 11, color: isFocused ? '#005BFF' : '#7D8799', fontWeight: !isFocused ? 'normal' : 'bold', fontFamily: 'OpenSans-Regular' }}>
                                {label}
                            </Text>
                        </TouchableOpacity>
                    </View>
                );
            })}
        </View>
    )
}



export default CustomTabBar;