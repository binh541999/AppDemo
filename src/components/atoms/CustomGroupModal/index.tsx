import { useSelector } from 'react-redux'
import color from '@config/colors';
import metric from '@config/metrics';
import CheckBox from '@react-native-community/checkbox';
import { deleteMemberFromGroup } from '@services/function/DeleteDB';
import { insertMemberToGroup } from '@services/function/insertDB';
import React, { Component, useEffect, useState } from 'react';
import { View, Modal, StyleSheet, Alert, Text, TouchableOpacity,
     TextInput, TouchableHighlight, KeyboardAvoidingView, Image, SafeAreaView} from 'react-native';
import { FlatList, ScrollView } from 'react-native-gesture-handler';


interface PopupConfig {
    visible: boolean,
    setVisible: any,
    employeeId:any,
    dataGroup: any,
}

const ChooseGroup =(props)=>{
    const [checked, setChecked] = useState(false);
    //
    const checkChecked = () => {
        let index = props.dataGroup.findIndex((item) => {
        console.log("🚀 ~ file: index.tsx ~ line 29 ~ index ~ item", item)    
          return item.idGroup == props.data.id 
        })
        setChecked(index > -1 ? true : false);
        // if (index > -1) {
        //   console.log("🚀 ~ file: index.tsx ~ line 30 ~ index ~ index", index)
        //   console.log("🚀 ~ file: index.tsx ~ line 23 ~ CustomChooseMember ~ currentIDGroup", currentIDGroup)
        //   console.log("🚀 ~ file: index.tsx ~ line 21 ~ CustomChooseMember ~ groupMember", groupMember)
        // }
      }
      useEffect(() => {
        //checkURL();
        //console.log("🚀 ~ file: index.tsx ~ line 29 ~ index ~ item", props)
        checkChecked();
        //console.log("🚀 ~ file: index.tsx ~ line 21 ~ CustomChooseMember ~ groupMember", groupMember)
        
      }, [])

    return (
        <SafeAreaView>
            <TouchableOpacity style={styles.itemStyle} onPress={() => {
                console.log("🚀 ~ file: index.tsx ~ line 48 ~ ChooseGroup ~ checked", props.data)
        {checked == false ?    
          insertMemberToGroup({ idGroup: props.data.id, idMember: props.employeeId })
        : 
          deleteMemberFromGroup({ idGroup: props.data.id, idMember: props.employeeId })
        }
        setChecked(!checked);
        
      }}>
                <Text>
                    {props.data.name}
                </Text>
                <CheckBox 
                    value={checked}
                    //onValueChange={(newValue) =>props.setChecked(newValue)}
                />
            </TouchableOpacity>
        </SafeAreaView>
    );
}



const PopupCollection = (props: PopupConfig) => {
    const renderItem = ({ item }) => (
        <ChooseGroup data={item} dataGroup={props.dataGroup} employeeId={props.employeeId} ></ChooseGroup>
    );
    //const groupMember = useSelector((state: any) => state?.groupMember)
    const group = useSelector((state: any) => state?.group)
    const [listShow,setListShow] = useState<any[]>([]);
    useEffect (()=>{
        if(group != undefined) {   
          setListShow(group);
        }     
       },[]);

    return (
            <Modal
                animationType="fade"
                transparent={true}
                visible={props.visible}
                >
                    <View style={styles.constrainOpacity}></View>          
                <TouchableOpacity           
                    style={styles.centeredView}
                    onPress={() => { props.setVisible(false) }}
                    activeOpacity={1}
                >   
                    <View style={styles.modalView}>                         
                        <FlatList renderItem={renderItem} 
                                data={listShow}
                                keyExtractor = { (item, index) => index.toString() }
                                horizontal={false}>

                        </FlatList>
                    </View>
                </TouchableOpacity>
            </Modal >
    );
}

const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22,
    },
    modalView: {
        width: metric.DEVICE_WIDTH - 48,
        height: metric.DEVICE_HEIGHT - 400,
        borderRadius: 12,
        padding: 20,
        shadowColor: "white",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        justifyContent:'center'

    },
   
    constrainOpacity: {
        backgroundColor: 'white',
        opacity: 0.85,
        position: 'absolute',
        top: 0,
        left: 0,
        height: metric.DEVICE_HEIGHT + 50,
        width: metric.DEVICE_WIDTH
    },
    itemStyle:{
        flexDirection:'row',
        justifyContent:'space-between',
        backgroundColor:'white',
        paddingHorizontal:15
    }
    
});

export default PopupCollection;