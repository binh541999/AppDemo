import React, { useState } from 'react';
import { View, StyleSheet,Image, Alert } from 'react-native';
import{
    Text,
} from 'react-native-paper'
import metric from '@config/metrics';
import { useSelector, useDispatch } from 'react-redux';
import { SafeAreaView } from 'react-native-safe-area-context';
import { TouchableOpacity } from 'react-native-gesture-handler';
import {IconLogout, IconRefresh,IconMessage} from '@assets/svg'
import fn_getContact from '@services/api/getContact';
import { setContact, setSignOut } from '@services/redux/actions';
import Loader from '../Loader';
import { dispatchContact } from '@services/function/login';
import { LOGIN } from '@config/constrains';
import { useNavigation } from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';

const CustomDrawerBar = () => {
    const navigation = useNavigation()
    const userInfo = useSelector((state: any) => state?.userInfo);
    const token = useSelector((state: any) => state?.token);
    const loading = useSelector((state: any) => state?.control.isLoading);
    const dispatch=useDispatch();
    const iconStyle ={
        width:25,
         height:25,
          color:'gray',
           fill:'none'}
    return (
       <SafeAreaView>
           <Loader loading={loading}/>
           <View style={styles.header}>
               <View style={styles.imageContainer}>
                    <Image style={styles.imageAvatar} resizeMode='stretch'
                    source={{uri:userInfo?.employeePicUrl}} />
               </View> 
               <View style={{paddingVertical:10}}>
                    <Text style={{color:'white', fontSize:16}}>
                            {userInfo?.fullName}
                    </Text>
                    <Text style={{color:'white'}}>
                            kms\{userInfo?.userName}
                    </Text>   
                </View>                                 
           </View>
           <View >
               <TouchableOpacity style={{height:60,alignItems:'center',paddingHorizontal:20,flexDirection:'row'}} 
               onPress={()=>{
                dispatchContact(token,userInfo.employeeCode);
                console.log("🚀 ~ file: index.tsx ~ line 51 ~ CustomDrawerBar ~ userInfo.employeeCode", userInfo.employeeCode)
                console.log("🚀 ~ file: index.tsx ~ line 51 ~ CustomDrawerBar ~ token", token)
                                    
                 
               }}>
                   <IconRefresh  iconStyle={iconStyle}/>
                   <View style={{paddingLeft:20}}>
                        <Text>
                            Update Contact
                        </Text>
                        {/* {
                            loading? 
                            <Text>
                                Updating
                            </Text> 
                            :
                            <Text>
                                {userInfo.lastUpdated}
                            </Text> 
                            
                        } */}
                        
                   </View>          
               </TouchableOpacity>
               <TouchableOpacity style={{height:60,alignItems:'center',paddingHorizontal:20,flexDirection:'row'}} onPress={()=>{
                   AsyncStorage.setItem('signOut', 'true')
                   
                   console.log('logout')
                   navigation.navigate(LOGIN)    
               }}>
                   <IconLogout  iconStyle={iconStyle}/>
                   <View style={{paddingLeft:20}}>
                        <Text>
                            Logout
                        </Text>
                        
                   </View>          
               </TouchableOpacity>

           </View>
       </SafeAreaView>
    )
}
const styles = StyleSheet.create({
    container: {
       height:metric.DEVICE_HEIGHT,
    },
    header: {
      backgroundColor: '#4591ed',
      height:metric.DEVICE_HEIGHT*0.2,
      paddingHorizontal: 15,
      paddingVertical: 15,
      fontSize: 20,
    },
    imageContainer: {           
        height:70,
        width:70,
        overflow:'hidden',
        borderRadius: Math.round(70 + 70) / 2,
      },
      imageAvatar: {          
        height:110,
        width:70,
        borderRadius: Math.round(70 + 70) / 2,
       
      },
  });



export default CustomDrawerBar;