import { useSelector } from 'react-redux';
import metric from '@config/metrics';
import { insertGroup, insertMemberToGroup } from '@services/function/insertDB';

import React, { Component, useEffect, useState } from 'react';
import {
    View, Modal, StyleSheet, Alert, Text, TouchableOpacity,
    TextInput, TouchableHighlight, KeyboardAvoidingView, Image, SafeAreaView
} from 'react-native';
import { FlatList, ScrollView } from 'react-native-gesture-handler';

interface PopupConfig {
    visible: boolean,
    setVisible: any,
}


const PopupCollection = (props: PopupConfig) => {
    const [groupName, setGroupName] = React.useState('');
    const group = useSelector((state: any) => state?.group);
    const createGroup = () => {
        if (groupName == '') {
            Alert.alert('Please insert valid name')
        }
        else {
            let index = group.findIndex((item) => {
                //console.log("🚀 ~ file: index.tsx ~ line 29 ~ index ~ item", item.idMember)    
                return item.name == groupName;
            })
            if (index == -1) {         
                insertGroup({ groupName: groupName }) 
                setGroupName('')
                { props.setVisible(false) }
            }
            else 
            {
                Alert.alert('Da co ten group nay ')
            }
        }
    }
    return (
        <Modal
            animationType="fade"
            transparent={true}
            visible={props.visible}
        >
            <View style={styles.constrainOpacity}></View>
            <TouchableOpacity
                style={styles.centeredView}
                onPress={() => { props.setVisible(false) }}
                activeOpacity={1}
            >
                <View style={styles.modalView}>
                    <View style={{ alignItems: 'center', margin: 10, }}>
                        <Text style={{ fontSize: 20 }}>
                            New Group
                            </Text>
                    </View>

                    <TextInput placeholder="Enter Name" value={groupName} style={{ width: metric.DEVICE_WIDTH - 88, height: 40, backgroundColor: '#c7c3c3' }}
                        onChangeText={text => setGroupName(text)}
                    />
                    <View style={{ flexDirection: 'row', padding: 15, alignItems: 'center', justifyContent: 'center' }}>
                        <TouchableOpacity style={styles.buttonStyle} onPress={() => { props.setVisible(false) }}>
                            <Text style={{ color: 'white' }}>
                                Cancel
                                </Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.buttonStyle} onPress={() => {
                            createGroup()

                        }}>
                            <Text style={{ color: 'white' }}>
                                Save
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </TouchableOpacity>
        </Modal >
    );
}

const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",

    },
    modalView: {
        width: metric.DEVICE_WIDTH - 48,
        height: metric.DEVICE_HEIGHT / 4,
        borderRadius: 12,
        padding: 20,
        shadowColor: "white",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,


    },

    constrainOpacity: {
        backgroundColor: 'white',
        opacity: 0.85,
        position: 'absolute',
        top: 0,
        left: 0,
        height: metric.DEVICE_HEIGHT + 50,
        width: metric.DEVICE_WIDTH
    },
    buttonStyle: {
        height: 45,
        width: 100,
        marginHorizontal: 10,
        backgroundColor: '#4591ed',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 8
    }

});

export default PopupCollection;