
import React, { memo, useCallback, useEffect, useState } from 'react';
import { View, TouchableOpacity, Text, StyleSheet, Image, Linking, Alert } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { IconPhone, ICON_HEIGHT, ICON_WIDTH } from '@assets/svg';
import { ImageAvatarDefault } from '@assets/images'


const CustomContact = (props: any) => {
  const [imageURL, setImageURL] = useState('');
  let Image_Http_URL = { uri: props.employeePicUrl };
  const iconStyle = {
    width: ICON_WIDTH,
    height: ICON_HEIGHT,
    color: 'red',
    fill: 'red'
  }
  //var url = 'https://hr.kms-technologgy.com/api/employees//600?code=ByOA7eZLSHF6L95iyGizFtjuYTOeEEmgFViH2V3kUDP0';
  //       Linking.canOpenURL(url).then(supported => {
  //           if (!supported) {
  //               console.log(supported)
  //           } else {
  //             console.log(supported)
  //           }
  //       }).catch(err => console.error('An error occurred', err));

  const checkURL =  () => {
     fetch(props.employeePicUrl).then((res) => {
      //console.log("🚀 ~ file: index.tsx ~ line 29 ~ fetch ~ props.employeePicUrl", props.employeePicUrl)
      if(res.ok) {
        //console.log("🚀 ~ file: index.tsx ~ line 52 ~ fetch ~ res", res)
        setImageURL(props.employeePicUrl);
      }
      
    }).catch(() => {
      setImageURL('')
    })
  }
  useEffect(() => {
    checkURL();
    
  }, [])

  return (
    <SafeAreaView>
      <TouchableOpacity>
        <View style={styles.itemContainer}>
          <View style={styles.leftElementContainer}>
            <View style={styles.imageContainer}>
              <Image style={styles.imageAvatar} resizeMode="stretch" source={ imageURL == '' ? ImageAvatarDefault: Image_Http_URL}
              //source={imageURL != '' ? { uri: imageURL } : ImageAvatarDefault} 
              />
            </View>
          </View>
          <View style={styles.rightSectionContainer}>
            <View style={styles.mainTitleContainer}>
              <Text
                style={
                  styles.nameStyle
                }>{props.shortName}</Text>
              <Text
                style={
                  styles.titleStyle
                }>{props.titleName}  </Text>
            </View>
            <TouchableOpacity style={styles.phoneContainer}>
              <IconPhone iconStyle={iconStyle} />
            </TouchableOpacity>
          </View>
        </View>
      </TouchableOpacity>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  itemContainer: {
    flexDirection: 'row',
    height: 75,
    borderColor: 'transparent',
    paddingHorizontal: 20,

  },
  leftElementContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  rightSectionContainer: {
    flexDirection: 'row',
    flex: 1,
    alignItems: 'center',
    paddingHorizontal: 10,

  },
  mainTitleContainer: {
    justifyContent: 'center',
    flexDirection: 'column',
    flex: 1,

  },
  nameStyle: {
    fontSize: 16,
    color: 'black',
  },
  titleStyle: {
    fontSize: 14,
  },
  phoneContainer: {
    flex: 0.1,
    alignItems: 'center'
  },
  imageContainer: {
    height: 60,
    width: 60,
    overflow: 'hidden',
    borderRadius: Math.round(50 + 50) / 2,
  },
  imageAvatar: {
    height: 80,
    width: 60,
    borderRadius: Math.round(50 + 50) / 2,

  },
});
export default CustomContact;

