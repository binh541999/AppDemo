import { useSelector } from 'react-redux'
import React, { memo, useCallback, useEffect, useState } from 'react';
import { View, TouchableOpacity, Text, StyleSheet, Image, Linking, Alert } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { IconPhone, ICON_HEIGHT, ICON_WIDTH } from '@assets/svg';
import { ImageAvatarDefault } from '@assets/images'
import CheckBox from '@react-native-community/checkbox';
import dboGroupMember from '@services/sqllite/dboGroupMember';
import { insertGroup, insertMemberToGroup } from '@services/function/insertDB';
import { deleteMemberFromGroup } from '@services/function/DeleteDB';


const CustomChooseMember = (props: any) => {

  const [imageURL, setImageURL] = useState('');
  let Image_Http_URL = { uri: props.employeePicUrl };
  const iconStyle = {
    width: ICON_WIDTH,
    height: ICON_HEIGHT,
    color: 'red',
    fill: 'red'
  }
  const groupMember = useSelector((state: any) => state?.groupMember)
  const currentIDGroup = useSelector((state: any) => state?.currentIDGroup)

  const [checked, setChecked] = useState(false);

  const checkChecked = () => {
    let index = groupMember.findIndex((item) => {
    //console.log("🚀 ~ file: index.tsx ~ line 29 ~ index ~ item", item.idMember)    
      return item.idGroup == currentIDGroup.id && item.idMember == props.employeeId
    })
    setChecked(index > -1 ? true : false);
    // if (index > -1) {
    //   console.log("🚀 ~ file: index.tsx ~ line 30 ~ index ~ index", index)
    //   console.log("🚀 ~ file: index.tsx ~ line 23 ~ CustomChooseMember ~ currentIDGroup", currentIDGroup)
    //   console.log("🚀 ~ file: index.tsx ~ line 21 ~ CustomChooseMember ~ groupMember", groupMember)
    // }
  }
  const checkURL =  () => {
    fetch(props.employeePicUrl).then((res) => {
     //console.log("🚀 ~ file: index.tsx ~ line 29 ~ fetch ~ props.employeePicUrl", props.employeePicUrl)
     if(res.ok) {
       //console.log("🚀 ~ file: index.tsx ~ line 52 ~ fetch ~ res", res)
       setImageURL(props.employeePicUrl);
     }
     
   }).catch(() => {
     setImageURL('')
   })
 }
 useEffect(() => {
   checkURL();
   checkChecked();
 }, [])

  return (
    <SafeAreaView>
      <TouchableOpacity onPress={() => {
        //console.log("🚀 ~ file: index.tsx ~ line 99 ~ CustomChooseMember ~ val truoc", checked)
        {checked == false ? 
          insertMemberToGroup({ idGroup: currentIDGroup.id, idMember: props.employeeId })
          //console.log("🚀 ~ file: index.tsx ~ line 21 ~ CustomChooseMember ~ groupMember", groupMember)
        
        : 
          deleteMemberFromGroup({ idGroup: currentIDGroup.id, idMember: props.employeeId })
          console.log("🚀 ~ file: index.tsx ~ line 21 ~ CustomChooseMember ~ groupMember", groupMember)
        }
        setChecked(!checked);
        //console.log("🚀 ~ file: index.tsx ~ line 99 ~ CustomChooseMember ~ val sau", checked)
        
      }}>
        <View style={styles.itemContainer}>
          <View style={styles.leftElementContainer}>
            <View style={styles.imageContainer}>
              <Image style={styles.imageAvatar} resizeMode="stretch"
                source={ imageURL == '' ? ImageAvatarDefault: Image_Http_URL}
              //source={imageURL != '' ? { uri: imageURL } : ImageAvatarDefault}
              />
            </View>
          </View>
          <View style={styles.rightSectionContainer}>
            <View style={styles.mainTitleContainer}>
              <Text
                style={
                  styles.nameStyle
                }>{props.shortName}</Text>
              <Text
                style={
                  styles.titleStyle
                }>{props.titleName}  </Text>
            </View>
            <CheckBox
              value={checked}
              onValueChange={(val:any) => {
               
              }}
              style={styles.phoneContainer}>
            </CheckBox>
          </View>
        </View>
      </TouchableOpacity>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  itemContainer: {
    flexDirection: 'row',
    height: 75,
    borderColor: 'transparent',
    paddingHorizontal: 20,

  },
  leftElementContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  rightSectionContainer: {
    flexDirection: 'row',
    flex: 1,
    alignItems: 'center',
    paddingHorizontal: 10,

  },
  mainTitleContainer: {
    justifyContent: 'center',
    flexDirection: 'column',
    flex: 1,

  },
  nameStyle: {
    fontSize: 16,
    color: 'black',
  },
  titleStyle: {
    fontSize: 14,
  },
  phoneContainer: {
    flex: 0.1,
    alignItems: 'center'
  },
  imageContainer: {
    height: 60,
    width: 60,
    overflow: 'hidden',
    borderRadius: Math.round(50 + 50) / 2,
  },
  imageAvatar: {
    height: 80,
    width: 60,
    borderRadius: Math.round(50 + 50) / 2,

  },
});
export default CustomChooseMember;

