import React from "react";
import Svg, { Path } from 'react-native-svg';

const IconLogOut=(props)=> {
  return (
    <Svg
    width={props.iconStyle.width}
    height={props.iconStyle.height}
    fill={props.iconStyle.fill}
    stroke={props.iconStyle.color}
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth="2"
      viewBox="0 0 24 24"
    >
      <Path d="M9 21H5a2 2 0 01-2-2V5a2 2 0 012-2h4"></Path>
      <Path d="M16 17L21 12 16 7"></Path>
      <Path d="M21 12L9 12"></Path>
    </Svg>
  );
}
export default IconLogOut;
