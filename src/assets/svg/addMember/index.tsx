import React, { Component } from 'react';
import Svg, { Circle, Path } from 'react-native-svg';

const IconAddMember = (props) => {
    return (
        <Svg
        width={props.iconStyle.width}
        height={props.iconStyle.height}
        fill={props.iconStyle.fill}
        stroke={props.iconStyle.color}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="2"
        viewBox="0 0 24 24"
      >
        <Path d="M16 21v-2a4 4 0 00-4-4H5a4 4 0 00-4 4v2"></Path>
        <Circle cx="8.5" cy="7" r="4"></Circle>
        <Path d="M20 8L20 14"></Path>
        <Path d="M23 11L17 11"></Path>
        </Svg>
    )
}

export default IconAddMember