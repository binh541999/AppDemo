import React from "react";
import Svg, { Path } from 'react-native-svg';

const IconRefresh=(props)=> {
  return (
    <Svg
    width={props.iconStyle.width}
    height={props.iconStyle.height}
    fill={props.iconStyle.fill}
    stroke={props.iconStyle.color}
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth="2"
      viewBox="0 0 24 24"
    >
      <Path d="M1 4L1 10 7 10"></Path>
      <Path d="M23 20L23 14 17 14"></Path>
      <Path d="M20.49 9A9 9 0 005.64 5.64L1 10m22 4l-4.64 4.36A9 9 0 013.51 15"></Path>
    </Svg>
  );
}
export default IconRefresh;
