import iconDownArrow from "./down-arrow/index"
import IconPhone from "./phone/index"
import IconMessage from "./message/index"
import IconRefresh from "./refresh/index"
import IconLogout from "./logOut/index"
import IconAddMember from "./addMember/index"
import IconMember from "./member/index"
import IconPlus from "./iconPlus/index"

export const ICON_HEIGHT = 24;
export const ICON_WIDTH = 24;
export {iconDownArrow,IconPhone,IconMessage,IconRefresh,IconLogout,IconAddMember,IconMember,IconPlus}
