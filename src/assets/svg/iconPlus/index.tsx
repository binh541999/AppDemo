import React from "react";
import Svg, { Path } from 'react-native-svg';

const IconPLus=(props)=> {
  return (
    <Svg
    width={props.iconStyle.width}
    height={props.iconStyle.height}
    fill="none"
    stroke="black"
    strokeLinecap="round"
    strokeLinejoin="round"
    strokeWidth="2"
    viewBox="0 0 24 24"
  >
    <Path d="M12 5L12 19"></Path>
    <Path d="M5 12L19 12"></Path>
  </Svg>
  );
}
export default IconPLus;
