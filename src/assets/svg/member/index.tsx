import React, { Component } from 'react';
import Svg, { Circle, Path } from 'react-native-svg';

const IconMember = (props) => {
    return (
        <Svg
        width={props.iconStyle.width}
        height={props.iconStyle.height}
        fill={props.iconStyle.fill}
        stroke={props.iconStyle.color}
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth="2"
      viewBox="0 0 24 24"
    >
      <Path d="M17 21v-2a4 4 0 00-4-4H5a4 4 0 00-4 4v2"></Path>
      <Circle cx="9" cy="7" r="4"></Circle>
      <Path d="M23 21v-2a4 4 0 00-3-3.87M16 3.13a4 4 0 010 7.75"></Path>
    </Svg>
    )
}

export default IconMember