import { Platform } from "react-native"

export const PLATFORM = {
    IS_IOS: Platform.OS == 'ios',
    IS_ANDROID: Platform.OS == "android",
}

export const CONTACT = 'Contact'
export const GROUPS = 'Groups'
export const LOGIN = 'Login'
export const DRAWER = 'Drawer'
export const DETAIL = 'Detail'
export const ADDMEMBER = 'AddMember'