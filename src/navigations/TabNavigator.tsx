import React, { Component } from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import Feather from 'react-native-vector-icons/Feather';
import { CustomTabBar } from '@components/atoms';
import {CONTACT,GROUPS,DETAIL, ADDMEMBER} from '@config/constrains';
import {Contact,Group,ContactDetail, AddMemberGroup} from '@scenes'


const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();
const HomeStackScreen = ({navigation}) => (
    <Stack.Navigator screenOptions={{
            headerStyle: {
            backgroundColor: '#1f65ff',
            },
            headerTintColor: '#fff',
            headerTitleAlign: 'center',
            headerTitleStyle: {
            fontWeight: 'bold',      
            textAlign: 'center',    
            }
        }}>
            <Stack.Screen name={CONTACT} component={Contact} options={{
            title:'Contact',
            headerLeft: () => (
                <Feather.Button name="menu" size={25} backgroundColor="#1f65ff" onPress={() => navigation.openDrawer()}></Feather.Button>
            )
            }} />
            <Stack.Screen name= {DETAIL} component={ContactDetail} />
    </Stack.Navigator>
    );
    
    const GroupStackScreen = ({navigation}) => (
    <Stack.Navigator initialRouteName={"Group"} screenOptions={{
            headerStyle: {
            backgroundColor: '#1f65ff',
            },
            headerTintColor: '#fff',
            headerTitleAlign: 'center',
            headerTitleStyle: {
            fontWeight: 'bold',      
            textAlign: 'center',  
            }
            
        }}>
            <Stack.Screen name="Group" component={Group} options={{
            headerLeft: () => (
                <Feather.Button name="menu" size={25} backgroundColor="#1f65ff" onPress={() => navigation.openDrawer()}></Feather.Button>
            )
            }} />
            <Stack.Screen name={ADDMEMBER} component={AddMemberGroup} />
            <Stack.Screen name= {DETAIL} component={ContactDetail} />
    </Stack.Navigator>
    );

const TabNavigator = () => {
    return (
        <Tab.Navigator tabBar={(props: any) => <CustomTabBar {...props} />}>
            <Tab.Screen name={CONTACT} component={HomeStackScreen} ></Tab.Screen>
            <Tab.Screen name={GROUPS} component={GroupStackScreen}></Tab.Screen>           
        </Tab.Navigator>
    )
}

export default TabNavigator;