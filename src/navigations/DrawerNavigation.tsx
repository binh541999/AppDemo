import * as React from 'react';
import { View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem,
} from '@react-navigation/drawer';
import TabNavigator from "./TabNavigator"
import { CustomDrawerBar } from '@components/atoms/';

const Drawer = createDrawerNavigator();

const DrawerNavigator = () => {
    return (
        <Drawer.Navigator drawerContent={props => <CustomDrawerBar />}>
          <Drawer.Screen name="Home" component={TabNavigator} />
      </Drawer.Navigator>
    );
  }
  
  export default DrawerNavigator;