import React, { Component, useEffect, useState } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import DrawerNavigator from './DrawerNavigation'
import { Group, Login } from '@scenes/index'
import { LOGIN, DRAWER, DETAIL, GROUPS } from '@config/constrains';


import { useSelector, useDispatch } from 'react-redux';
import storage from '@config/initStorage';
import AsyncStorage from '@react-native-async-storage/async-storage';
import dboMember from '@services/sqllite/dboMember';
import dboGroupMember from '@services/sqllite/dboGroupMember';
import dboGroup from '@services/sqllite/dboGroup';
import { setContact, setGroup, setGroupMember, setToken, setSignOut,setUserInfo } from '@services/redux/actions';

const Stack = createStackNavigator();

export var dispatch: any;


const Navigator = () => {
    const [firstLoad, setFirstLoad] = useState<any>(null);
    const [isSignOut, setSignOut] = useState<any>(null);

    dispatch = useDispatch();
    // useEffect(()=>{
    //     const user = ()=>{ 
    //         storage.load({
    //           key: 'user',
    //           autoSync:true,
    //         })
    //         .then(res => {

    //         }).catch(error =>{

    //             console.log("🚀 ~ file: index.tsx ~ line 25 ~ user ~ error", error)
    //         }

    //         )}
    //         user();
    // },[])
    useEffect(() => {
       // console.log("🚀 ~ file: index.tsx ~ line 24 ~ Navigator ~ isSignOut", isSignOut)
        AsyncStorage.getItem('signOut').then(res => {
//console.log("🚀 ~ file: index.tsx ~ line 45 ~ AsyncStorage.getItem ~ res", res)
            if (res != null) {
                //console.log("🚀 ~ file: index.tsx ~ line 47 ~ AsyncStorage.getItem ~ res", res)
                setSignOut(res);
            }
            else {
                setSignOut(true);
                AsyncStorage.setItem('signOut', 'true')
            }
        })
        AsyncStorage.getItem('firstOpen').then(res => {
            if (res == null) {
                AsyncStorage.setItem('firstOpen', 'true')
                setFirstLoad(true)
            }
            else {
                setFirstLoad(false)

            }
           
        })
    }, [])

    const loadData = () => {


        let task1 = dboMember.SelectAll().then((res) => {
            //console.log("🚀 ~ file: index.tsx ~ line 57 ~ task1 ~ res", res)
            dispatch(setContact(res));
            AsyncStorage.getItem('userCode').then(item => {
                let index = res.findIndex((a) => {

                    return a.employeeCode == item
                })              
                dispatch(setUserInfo(res[index]));    
            })
            AsyncStorage.getItem('token').then(item => {
                           
                dispatch(setToken(item));    
            })
        })
        let task2 = dboGroup.SelectAll().then((res) => {
            dispatch(setGroup(res));
            
        })
        let task3 = dboGroupMember.SelectAll().then((res) => {
            dispatch(setGroupMember(res));
        })
        Promise.all([task1, task2, task3]).catch
    }
   

    if (isSignOut == null)
        
        return null;
    else {
        if (firstLoad == true) {
            //console.log('first Load')
            let task1 = dboMember.CreateTable();
            let task2 = dboGroup.CreateTable();
            let task3 = dboGroupMember.CreateTable();
            Promise.all([task1, task2, task3])
        }
        else {
            //console.log("🚀 ~ file: index.tsx ~ line 85 ~ Navigator ~ isSignOut", isSignOut)
            //console.log('first Load 2')
            loadData();
        }

        return (
            <NavigationContainer>
                {/* <Stack.Navigator initialRouteName='Tab' headerMode="none">
                <Stack.Screen name='Tab' component={TabNavigator}></Stack.Screen>
            </Stack.Navigator> */}

                <Stack.Navigator initialRouteName={isSignOut == true ? LOGIN : DRAWER} headerMode="none" >
                    <Stack.Screen name={LOGIN} component={Login} />
                    <Stack.Screen name={DRAWER} component={DrawerNavigator} />
                </Stack.Navigator>



            </NavigationContainer>
        )
    }
}


export default Navigator;