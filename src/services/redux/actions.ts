import {SET_TOKEN,SET_CONTACT,SET_USER_INFO, LOADING, SET_MEMBER, SET_GROUP,  ADD_GROUP_MEMBER, SET_GROUP_MEMBER, ADD_GROUP, CURRENT_GROUP, SET_GROUP_MEMBER_CONTACT, SET_SIGN_OUT} from './constrains'
const setToken = (token) =>{
    return {
        type:SET_TOKEN,
        payload:token,
    };
};

const setUserInfo = (data) =>{
    return {
        type:SET_USER_INFO,
        payload:data,
    };
}

const setContact = (data) =>{
    return {
        type:SET_CONTACT,
        payload:data,
    };
}

const loading = (data) =>{
    return {
        type:LOADING,
        payload:data,
    };
}

const addGroup = (data) =>{
    return {
        type:ADD_GROUP,
        payload:data,
    };
}


const setMember = (data) =>{
    return {
        type:SET_MEMBER,
        payload:data,
    };
}
const setGroup = (data) =>{
    return {
        type:SET_GROUP,
        payload:data,
    };
}
const addGroupMember = (data) =>{
    return {
        type:ADD_GROUP_MEMBER,
        payload:data,
    };
}

const setGroupMember = (data) =>{
    return {
        type:SET_GROUP_MEMBER,
        payload:data,
    };
}

const setCurrentGroup = (data) =>{
    return {
        type:CURRENT_GROUP,
        payload:data,
    };
}

const setGroupMemberContact = (data) =>{
    return {
        type:SET_GROUP_MEMBER_CONTACT,
        payload:data,
    };
}

const setSignOut = (data) =>{
    return {
        type:SET_SIGN_OUT,
        payload:data,
    };
}



export {setToken,setContact,setUserInfo, setGroupMemberContact,setSignOut,
    loading,addGroup,setGroup,addGroupMember,setMember,setGroupMember,setCurrentGroup
};