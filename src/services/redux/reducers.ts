import AsyncStorage from '@react-native-async-storage/async-storage';
import { SET_TOKEN, SET_CONTACT, SET_USER_INFO, LOADING, ADD_GROUP, SET_MEMBER, SET_GROUP, ADD_GROUP_MEMBER, SET_GROUP_MEMBER, CURRENT_GROUP, SET_GROUP_MEMBER_CONTACT, SET_SIGN_OUT } from './constrains';
var initState = {
    token:"",
    // userInfo:{name:''},
    userInfo:{},
    contactList:undefined,
    control:{
        isLoading:false,
        isSignOut:true,
        
    },
    member:[],
    group:[],
    groupMember:[],
    groupMemberContact:[],
    currentIDGroup: [],
    
};



export const rootReducer = (state: any = initState, action: any) => {
    switch (action.type) {
        case SET_TOKEN:{
            return {
                ...state,
                token:action.payload,
            }
        }
        case SET_USER_INFO:{
            return {
                // ...state,
                // userInfo:{
                //     ...state.userInfo,name:action.payload
                // },
                ...state,
                userInfo:action.payload
            }
        }
        case SET_CONTACT:{
            return {
                ...state,
                contactList:action.payload,
            }
        }
        case LOADING:{
            return {
                ...state,
                control:{
                    ...state.control,
                    isLoading:action.payload},
            }
        }

        case SET_SIGN_OUT:{
            
            return {
                
                ...state,
                control:{
                    ...state.control,
                    isSignOut:action.payload},
            }
        }

        case ADD_GROUP:{
            return {
                ...state,
                group:[
                    ...state.group,
                     action.payload
                ],
            }
        }

        case SET_MEMBER:{
            return {
                ...state,
                member:action.payload,
            }
        }

        case SET_GROUP:{
            return {
                ...state,
                group:action.payload,
            }
        }

        case SET_GROUP_MEMBER:{
            return {
                ...state,
                groupMember:action.payload,
            }
        }

        case SET_GROUP_MEMBER_CONTACT:{
            return {
                ...state,
                groupMemberContact:action.payload,
            }
        }

        case ADD_GROUP_MEMBER:{
            return {
                ...state,
                groupMember:[
                    ...state.groupMember,
                    action.payload],
            }
        }

        case CURRENT_GROUP:{
            return {
                ...state,
                currentIDGroup:action.payload,
            }
        }

       
    }
    
}