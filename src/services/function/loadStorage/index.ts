import { dispatch } from '@navigations';
import storage from "@config/initStorage";


const loadListGroup =  (dispatch) =>{
     storage.load({
              key: 'listGroup',
              autoSync:true,
            }).then((res)=>{
                dispatch(loadListGroup(res))
            }).catch(error =>{               
                console.log( error)
            }
            )
}

export {loadListGroup}