
import dboMember from '@services/sqllite/dboMember';
import fn_getContact from "@services/api/getContact";
import fn_Login from "@services/api/login";
import { loading, setContact, setSignOut, setToken, setUserInfo } from "@services/redux/actions";
import { Alert } from "react-native";
import storage from "@config/initStorage";
import { dispatch } from "@navigations";
import AsyncStorage from '@react-native-async-storage/async-storage';


const dispatchContact = (token, userCode) => {
    return new Promise((resolve, reject) => {
        fn_getContact(token, (res) => {
            if (res == "error") {
                reject({ status: 500, error: 'error' })
            }
            else {
                res.items.sort(
                    (a, b) =>
                        a.shortName.toLowerCase() > b.shortName.toLowerCase(),
                );
                res.items.forEach(element => {
                    dboMember.InsertItem({
                        employeeId: element.employeeId,
                        skype: element.skype,
                        email: element.email,
                        userName: element.userName,
                        employeeCode: element.employeeCode,
                        currentOffice: element.currentOffice,
                        currentOfficeFullName: element.currentOfficeFullName,
                        empVietnameseName: element.empVietnameseName,
                        titleName: element.titleName,
                        mobilePhone: element.mobilePhone,
                        employeePicUrl: element.employeePicUrl,
                        fullName: element.fullName,
                        shortName: element.shortName,
                    })
                });
                let index = res.items.findIndex((a) => {

                    return a.employeeCode == userCode
                })
                AsyncStorage.setItem('signOut', 'false')
                AsyncStorage.setItem('userCode', userCode)
                dispatch(setUserInfo(res.items[index]));
                //console.log("🚀 ~ file: index.ts ~ line 73 ~ fn_getContact ~ res.items[index]", res.items[index])
                dispatch(setContact(res.items));
                resolve({ status: 200 })
            }
        })

    });
}
const uploadTableDB = (token, userCode) => {
    dboMember.SelectAll().then((res: any) => {
        if (res.length == 0) {
            //console.log("🚀 ~ file: index.ts ~ line 69 ~ dboMember.SelectAll ~ userCode", userCode)
            dispatchContact(token, userCode);
        }
    })

}

const login = (username, password) => {
    return new Promise((resolve, reject) => {
        dispatch(loading(true));
        fn_Login(username, password, async (res) => {
            //console.log("🚀 ~ file: index.ts ~ line 60 ~ fn_Login ~ res", res)

            if (res == "error") {
            
                dispatch(loading(false));
                reject({ status: 500, error: 'error' })
            }
            else {
                AsyncStorage.setItem('token', res.token)
                uploadTableDB(res.token, res.employeeCode);
                //dispatch(setUserInfo(userInfo));
                dispatch(loading(false));
                //dispatch(setSignOut(false));
                resolve({ status: 200 })
            }
        });
    })


}

export { login, dispatchContact }