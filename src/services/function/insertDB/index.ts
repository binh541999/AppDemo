import  dboMember from '@services/sqllite/dboMember';
import dboGroup  from '@services/sqllite/dboGroup';
import dboGroupMember from '@services/sqllite/dboGroupMember';
import { dispatch } from "@navigations";
import { addGroup, addGroupMember, setContact, setGroup, setGroupMember } from '@services/redux/actions';

const insertGroup =(prop:any)=>{
    dboGroup.InsertItem({
        name:prop.groupName,
    }).then(()=>{
        dboGroup.SelectAll().then((res)=>{  
            console.log("🚀 ~ file: index.ts ~ line 12 ~ dboGroup.SelectAll ~ res", res)
            dispatch(setGroup(res));
        })   
    }
    )
}

const insertMemberToGroup =(prop:any)=>{
    dboGroupMember.InsertItem({
        idGroup:prop.idGroup,
        idMember:prop.idMember
    }).then(()=>{
        dboGroupMember.SelectAll().then((res)=>{
            //console.log("🚀 ~ file: index.ts ~ line 24 ~ dboGroupMember.SelectAll ~ res", res)
            dispatch(setGroupMember(res));
        })
    })
   
}


export {insertGroup,insertMemberToGroup}
