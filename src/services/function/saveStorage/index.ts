import storage from "@config/initStorage";

const saveUserAccount = async (account) =>{
    storage.save({
        key: 'user', // Note: Do not use underscore("_") in key!
        data: account,
        expires: null,
    });
}

const saveListGroup = async (group) =>{
    storage.save({
        key: 'listGroup', // Note: Do not use underscore("_") in key!
        data: group,
        expires: null,
    });
}

export {saveUserAccount,saveListGroup}