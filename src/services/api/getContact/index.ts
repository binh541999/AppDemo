
const fn_getContact = async (token,callback) =>{
  var myHeaders = new Headers();
  myHeaders.append("Authorization", "Bearer "+ token);
  var requestOptions = {
    method: 'GET',
    headers: myHeaders,
    redirect: 'follow'
};
 await fetch("https://hr.kms-technology.com/api/Contact/ReturnContactList/0/0", requestOptions)
  .then(response => response.text())
  .then(result => 
    callback(JSON.parse( result))
    )
  .catch(error => callback('error', error));
}
export default fn_getContact;