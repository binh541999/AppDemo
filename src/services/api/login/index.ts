import { saveUserAccount } from "@services/function/saveStorage";

var myHeaders = new Headers();
myHeaders.append("Content-Type", "application/json");
myHeaders.append("Cookie", "UserName=binhtatnguyen");


const fn_Login = async (username,password,callback)=>{
    
    var raw = JSON.stringify({"username":username,"password":password,"rememberMe":true});

    var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: raw,
    redirect: 'follow'
    };
   await fetch("https://home.kms-technology.com/api/Account/login", requestOptions)
  .then(response =>  response.text())
  
  .then(result => 
      {
        saveUserAccount({
          username:username,
          password:password});
        callback(JSON.parse( result))
    }
     
    )
  .catch(error => 
    callback("error")
    );
}
 export default fn_Login;
