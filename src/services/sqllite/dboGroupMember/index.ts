import SQLite from "react-native-sqlite-storage";
const DATABASE_NAME = 'data.db';


interface CollectionTable {
    idGroup: number,
    idMember: number
}

const CreateTable = () => {
    let query = `CREATE TABLE GroupMember (id INTEGER PRIMARY KEY AUTOINCREMENT,
                                        idGroup INTEGER , 
                                         idMember INTEGER )`;

    return new Promise((resolve, reject) => {
        SQLite.openDatabase({ name: DATABASE_NAME })
            .then((res) => {
                res.executeSql(query, [])
                    .then(() => {
                        resolve({ status: 200 })
                    }).catch(err => {
                        reject({ status: 500, error: err })
                    })
            })
            .catch(err => {
                reject(err)
            })
    })
}

const InsertItem = (props: CollectionTable) => {
    let query = 'INSERT INTO GroupMember (idGroup,idMember) VALUES (?,?)';

    return new Promise((resolve, reject) => {
        SQLite.openDatabase({ name: DATABASE_NAME })
            .then((res) => {
                res.executeSql(query, [props.idGroup, props.idMember])
                    .then((res) => {
                        resolve({ status: 200, data: res })
                    })
                    .catch((error) => {
                        // reject({ status: 500, error: "Error insert database" })
                        reject({ status: 500, error: error })
                    })
            })
            .catch(() => {
                reject({ status: 500, error: "Error insert open group Member database" })
            })
    })
}

const SelectAll = () => {
    return new Promise((resolve, reject) => {
        SQLite.openDatabase({ name: DATABASE_NAME })
            .then((res) => {
                res.transaction((tx) => {
                    tx.executeSql('SELECT * FROM GroupMember', [])
                        .then(([tx, result]) => {
                            let data: any[] = []
                            for (let i = 0; i < result.rows.length; i++) {
                                let row = result.rows.item(i);
                                data.push(row)
                            }
                            resolve(data)
                        }).catch(err => {
                            reject({ status: 500, error: "Error select groupmember database " + err})
                        })
                }).catch(err => {
                    reject({ status: 500, error: "Error transaction groupmember database "+ err })
                })
            }).catch(err => {
                reject({ status: 500, error: "Error opendb groupmember database "+ err })
            })
    })
}

// const Rename = (name: String, id: number) => {
//     let query = 'UPDATE Collection SET name = ? WHERE ID = ?;';
//     return new Promise((resolve, reject) => {
//         SQLite.openDatabase({ name: DATABASE_NAME })
//             .then((res) => {
//                 res.executeSql(query, [name, id])
//                     .then(() => {
//                         resolve({ status: 200 })
//                     })
//                     .catch(() => {
//                         reject({ status: 500, error: "Error insert database" })
//                     })
//             })
//             .catch(() => {
//                 reject({ status: 500, error: "Error insert database" })
//             })
//     })
// }

const DeleteItem = (props: CollectionTable) => {
    if (props.idGroup != null && props.idMember != null) {
       // console.log('Fucntion delete member');
        let query = 'DELETE FROM GroupMember WHERE idGroup = ? AND idMember = ?;';
        return new Promise((resolve, reject) => {
            //console.log('Fucntion delete member');
            SQLite.openDatabase({ name: DATABASE_NAME })
                .then((res) => {
                    // console.log("🚀 ~ file: index.ts ~ line 104 ~ .then ~ res", res)
                    // console.log("🚀 ~ file: index.ts ~ line 104 ~ .then ~ res", query)
                    // console.log('Fucntion delete member 4');
                    res.executeSql(query, [props.idGroup, props.idMember])
                        .then(() => {
                            //console.log('Fucntion delete success member');
                            resolve({ status: 200 })
                            
                           // dboMusic.DeleteItemByCollectionID(id)
                        })
                        .catch(() => {
                            //console.log('Fucntion delete erroe member');
                            reject({ status: 500, error: "Error insert database" })
                        })
                })
                .catch(() => {
                    //console.log('Fucntion delete error member');
                    reject({ status: 500, error: "Error insert database" })
                })
        })
    }
    return new Promise((resolve, reject) => {
        resolve(500)
    })
}

const DeleteData = () => {
    if (true) {
       // console.log('Fucntion delete member');
        let query = 'DELETE FROM GroupMember ;';
        return new Promise((resolve, reject) => {
        
            SQLite.openDatabase({ name: DATABASE_NAME })
                .then((res) => {
                    // console.log("🚀 ~ file: index.ts ~ line 104 ~ .then ~ res", res)
                    // console.log("🚀 ~ file: index.ts ~ line 104 ~ .then ~ res", query)
                    // console.log('Fucntion delete member 4');
                    res.executeSql(query, [])
                        .then(() => {
                            //console.log('Fucntion delete success member');
                            resolve({ status: 200 })
                            
                           // dboMusic.DeleteItemByCollectionID(id)
                        })
                        .catch(() => {
                            //console.log('Fucntion delete erroe member');
                            reject({ status: 500, error: "Error insert database" })
                        })
                })
                .catch(() => {
                    //console.log('Fucntion delete error member');
                    reject({ status: 500, error: "Error insert database" })
                })
        })
    }
    return new Promise((resolve, reject) => {
        resolve(500)
    })
}

const dboGroupMember = {
    InsertItem,
    SelectAll,
    CreateTable,
    DeleteItem,
    DeleteData
}

export default dboGroupMember;