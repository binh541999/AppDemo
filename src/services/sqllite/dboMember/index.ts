import SQLite from "react-native-sqlite-storage";
const DATABASE_NAME = 'data.db';


interface CollectionTable {
    employeeId:number
    skype: string,
    email:string,
    userName:string,
    employeeCode:string,
    currentOffice:string,
    currentOfficeFullName:string,
    empVietnameseName:string,
    titleName:string,
    mobilePhone:string,
    employeePicUrl:string,
    fullName:string,
    shortName:string,
}

const CreateTable = () => {
    let query = `CREATE TABLE Member (id INTEGER PRIMARY KEY AUTOINCREMENT, 
                                        employeeId INTEGER,
                                        skype VARCHAR(255),
                                        email VARCHAR(255),
                                        userName VARCHAR(255),
                                        employeeCode VARCHAR(255),
                                        currentOffice VARCHAR(255),
                                        currentOfficeFullName VARCHAR(255),
                                        empVietnameseName VARCHAR(255),
                                        titleName VARCHAR(255),
                                        mobilePhone VARCHAR(255),
                                        employeePicUrl VARCHAR(255),
                                        fullName VARCHAR(255),
                                        shortName VARCHAR(255) )                                  
                                        `;

    return new Promise((resolve, reject) => {
        SQLite.openDatabase({ name: DATABASE_NAME })
            .then((res) => {
                res.executeSql(query, [])
                    .then(() => {
                        resolve({ status: 200 })
                    }).catch(err => {
                        reject({ status: 500, error: err })
                    })
            })
            .catch(err => {
                reject(err)
            })
    })
}

const InsertItem = (props: CollectionTable) => {
    let query = `INSERT INTO Member (employeeId,skype,email,userName,employeeCode,
                                        currentOffice,currentOfficeFullName,empVietnameseName,
                                        titleName,mobilePhone,employeePicUrl,fullName,shortName
        ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)`;

    return new Promise((resolve, reject) => {
        SQLite.openDatabase({ name: DATABASE_NAME })
            .then((res) => {
                res.executeSql(query, [props.employeeId, props.skype,props.email, props.userName,
                                        props.employeeCode, props.currentOffice,props.currentOfficeFullName, 
                                        props.empVietnameseName,props.titleName, props.mobilePhone,
                                        props.employeePicUrl, props.fullName,props.shortName])
                    .then((res) => {
                        resolve({ status: 200, data: res })
                    })
                    .catch(() => {
                        reject({ status: 500, error: "Error insert database" })
                    })
            })
            .catch(() => {
                reject({ status: 500, error: "Error insert database" })
            })
    })
}

const SelectAll = () => {
    return new Promise((resolve, reject) => {
        SQLite.openDatabase({ name: DATABASE_NAME })
            .then((res) => {
                res.transaction((tx) => {
                    tx.executeSql('SELECT * FROM Member', [])
                        .then(([tx, result]) => {
                            let data: any[] = []
                            for (let i = 0; i < result.rows.length; i++) {
                                let row = result.rows.item(i);
                                data.push(row)
                            }
                            resolve(data)
                        }).catch(err => {
                            reject({ status: 500, error: "Error select member database" })
                        })
                }).catch(err => {
                    reject({ status: 500, error: "Error transaction member database" })
                })
            }).catch(err => {
                reject({ status: 500, error: "Error open member database" })
            })
    })
}



// const DeleteItem = (id: number) => {
//     if (id != 1) {
//         let query = 'DELETE FROM Collection WHERE ID = ?;';
//         return new Promise((resolve, reject) => {
//             SQLite.openDatabase({ name: DATABASE_NAME })
//                 .then((res) => {
//                     res.executeSql(query, [id])
//                         .then(() => {
//                             resolve({ status: 200 })
//                            // dboMusic.DeleteItemByCollectionID(id)
//                         })
//                         .catch(() => {
//                             reject({ status: 500, error: "Error insert database" })
//                         })
//                 })
//                 .catch(() => {
//                     reject({ status: 500, error: "Error insert database" })
//                 })
//         })
//     }
//     return new Promise((resolve, reject) => {
//         resolve(500)
//     })
// }

const dboMember = {
    InsertItem,
    SelectAll,
    CreateTable,
}

export default dboMember;