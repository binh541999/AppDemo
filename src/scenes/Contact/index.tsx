import React, { useState, useEffect } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  View,
  FlatList,
  Alert,
  Linking,
  Text
} from 'react-native';
import { Searchbar } from 'react-native-paper';
import { CustomContact } from '@components/atoms';
import metric from '@config/metrics';
import { useSelector, useDispatch } from 'react-redux';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { setContact, setUserInfo } from '@services/redux/actions';
import fn_getContact from '@services/api/getContact';
import { useNavigation } from '@react-navigation/native';
import { DETAIL } from '@config/constrains';


const Contact = () => {
  const navigation = useNavigation();
  const [listShow, setListShow] = useState<any[]>([]);
  const renderItem = ({ item }) => (
    <TouchableWithoutFeedback onPress={() => {
      //console.log("🚀 ~ file: index.tsx ~ line 41 ~ Contact ~ item", item)
      navigation.navigate(DETAIL, { item: item });
    }}>
      <CustomContact shortName={item.shortName} phoneNumber={item.phoneNumber}
        titleName={item.titleName} employeePicUrl={item.employeePicUrl} />
    </TouchableWithoutFeedback>

  );

  const [searchQuery, setSearchQuery] = useState('');

  const userInfo = useSelector((state: any) => state?.userInfo);
  const contactList = useSelector((state: any) => state?.contactList)
  const dispatch = useDispatch();
  const search = (text: string) => {

    setListShow(contactList.filter((val: any) => {
      if (val.shortName != '' && val.shortName.toLowerCase().search(text.toLocaleLowerCase()) > -1)
        return true;
      return false;
    }))
    if (text == '')
      setListShow(contactList);
  }

  useEffect(() => {
    if (contactList != undefined) {
      // console.log("🚀 ~ file: index.tsx ~ line 55 ~ useEffect ~ userInfo", userInfo)
      // if (userInfo != null) { 
        
      //   let index = contactList.findIndex((a) => {
        
      //     return a.employeeCode == userInfo.employeeCode
      //   })
      //   console.log("🚀 ~ file: index.tsx ~ line 57 ~ index ~ contactList", contactList[index])
      //   dispatch(setUserInfo(contactList[index]));
      // }

      setListShow(contactList);
    }
  }, [contactList]);

  return (
    <SafeAreaView>
      <View style={styles.container}>
        <Searchbar
          style={styles.searchBar}
          placeholder="name, title, phone number"
          onChangeText={(value) => {
            search(value);
            setSearchQuery(value)

          }}
          value={searchQuery}
        />
        <FlatList
          data={listShow}
          renderItem={renderItem}
          horizontal={false}
          keyExtractor={(item: any) =>
            item.employeeCode} />


      </View>

    </SafeAreaView>

  )
}

const styles = StyleSheet.create({
  container: {
    height: metric.DEVICE_HEIGHT,
  },
  header: {
    backgroundColor: '#4591ed',
    color: 'white',
    paddingHorizontal: 15,
    paddingVertical: 15,
    fontSize: 20,
  },
  searchBar: {
    marginHorizontal: 15,
    marginVertical: 5,
    fontSize: 12
  },
  sectionHeaderContainer: {
    height: 30,
    backgroundColor: 'gray',
    justifyContent: 'center',
    paddingHorizontal: 15,
  },

  sectionHeaderLabel: {
    color: 'white',
  },
});

export default Contact;