import React, { useState, useEffect } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  Image,
  Linking,
  TouchableOpacity,
  PermissionsAndroid,
} from 'react-native';
import { useSelector } from 'react-redux';
import { IconPhone, IconMessage } from '@assets/svg';
import Contacts from 'react-native-contacts';
import { CustomGroupModal } from '@components/atoms';
import { ImageAvatarDefault } from '@assets/images';

const ContactDetail = (props: any) => {
  const [imageURL, setImageURL] = useState('');
  const [modalVisible, setModalVisible] = useState(false);
  const item = props.route.params.item;
  let Image_Http_URL = { uri: item.employeePicUrl };
  const iconStyle = {
    width: 24,
    height: 24,
    color: 'red',
    fill: 'red'
  }
  const requestContactPermission = async () => {
    try {
      const granted = await PermissionsAndroid.requestMultiple(
        [PermissionsAndroid.PERMISSIONS.WRITE_CONTACTS,
        PermissionsAndroid.PERMISSIONS.READ_CONTACTS]
      );
      return granted;
    } catch (err) {
      console.warn(err);
    }
  };
  const group = useSelector((state: any) => state?.group)  
  
  const groupMember = useSelector((state: any) => state?.groupMember)  
  const [dataGroup,setDataGroup] = useState<any[]>([]);
  
  const loadDataGroups = ()=>{
    setDataGroup( groupMember.filter((obj)=>{
      return obj.idMember==item.employeeId
    }))
    
    }
    useEffect(() => {
      if (group.length != 0) {   
        //console.log("🚀 ~ file: index.tsx ~ line 55 ~ fetch ~ props.employeePicUrl", item)
        fetch(item.employeePicUrl).then((res) => {
          if(res.ok) {
            //console.log("🚀 ~ file: index.tsx ~ line 52 ~ fetch ~ res", res)
            setImageURL(item.employeePicUrl);
          }
          
         
        }).catch((error) => {
          //console.log("🚀 ~ file: index.tsx ~ line 144 ~ fetch ~ error", error)
          //setImageURL(ImageAvatarDefault);
          //console.log(imageURL);
        })
        // console.log("🚀 ~ file: index.tsx ~ line 39 ~ ContactDetail ~ group", group)   
        // console.log("🚀 ~ file: index.tsx ~ line 46 ~ index ~ groupMember", groupMember.filter((obj)=>{
        //   return obj.idMember==item.employeeId
        // }))
        loadDataGroups();
        //console.log("🚀 ~ file: index.tsx ~ line 41 ~ ContactDetail ~ dataGroup", dataGroup)
      }


  }, [group])
  
  

  return (
    <SafeAreaView style={styles.container}>
      <CustomGroupModal visible={modalVisible} setVisible={setModalVisible} dataGroup={dataGroup} employeeId={item.employeeId} />
      <View>
        <View style={styles.infoContainer}>
          <View style={styles.leftElementContainer}>
            <View style={styles.imageContainer}>
              <Image style={styles.imageAvatar} resizeMode='stretch' source={imageURL == '' ?ImageAvatarDefault: Image_Http_URL }>
              </Image>
            </View>


          </View>
          <View style={styles.rightSectionContainer}>
            <View style={styles.mainTitleContainer}>
              <Text style={styles.nameStyle}>
                {item.fullName} </Text>
              <Text style={styles.titleStyle}>
                {item.titleName} </Text>
              <Text style={styles.titleStyle}>
                {item.employeeCode}</Text>
            </View>
          </View>
        </View>
        <View style={styles.contactContainer}>
          <View >
            <Text>Phone</Text>
            <Text>{item.mobilePhone}</Text>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <TouchableOpacity style={styles.phoneContainer}>
              <IconMessage iconStyle={iconStyle} />
            </TouchableOpacity>
            {item.mobilePhone ?
              <TouchableOpacity style={styles.phoneContainer} onPress={() => {
                Linking.openURL(`tel:${item.mobilePhone}`)
              }}>
                <IconPhone iconStyle={iconStyle} />
              </TouchableOpacity>
              : null}
          </View>
        </View>
        <View style={styles.contactContainer}>
          <TouchableOpacity onPress={() => {
            Linking.openURL(`mailto:${item.email}`)
          }}>
            <Text style={styles.titleStyle}>Email</Text>
            <Text style={styles.titleStyle}>{item.email}</Text>
          </TouchableOpacity>
        </View>

        <View style={styles.contactContainer}>
          <TouchableOpacity onPress={() => {
            Linking.openURL(`skype:${item.email}?chat`)
          }}>
            <Text style={styles.titleStyle}>Skype</Text>
            <Text style={styles.titleStyle}>{item.skype}</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.contactContainer}>
          <View >
            <Text>Location</Text>
            <Text>{item.currentOfficeFullName}</Text>
          </View>
        </View>
        <View style={styles.shareContainer}>
          <TouchableOpacity style={styles.contactContainer}>
            <Text style={{ fontSize: 16 }}>Share Contact</Text>
          </TouchableOpacity>

          <TouchableOpacity style={styles.contactContainer} onPress={() => {
            requestContactPermission().then(granted => {
              if (granted?.['android.permission.READ_CONTACTS']
                && granted['android.permission.WRITE_CONTACTS'] === 'granted') {
          
                Contacts.getAll;
                var newPerson = {

                  company: 'KMS Technology, Inc',
                  emailAddresses: [{
                    label: 'work',
                    email: item.email,
                  }],
                  familyName: item.firstName,
                  middleName: item.middleName,
                  displayName: item.fullName,
                  givenName: item.fullName,
                  jobTitle: item.titleName,
                  phoneNumbers: [{
                    label: 'mobile',
                    number: item.mobilePhone,
                  }],
                  hasThumbnail: true,
                  thumbnailPath: Image_Http_URL.uri,
                }
                Contacts.openContactForm(newPerson).then(contact => {
                })
              }

            }
            )
          }}>
            <Text style={{ fontSize: 16 }}>Add Phone Contact</Text>
          </TouchableOpacity>

          <TouchableOpacity style={styles.contactContainer} onPress={() => {
            loadDataGroups();
            setModalVisible(true);
          }
          }>
            <Text style={{ fontSize: 16 }}>Group</Text>
          </TouchableOpacity>

        </View>


      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 15,
  },
  infoContainer: {
    flexDirection: 'row',
    height: 130,
    borderColor: 'transparent',

  },
  leftElementContainer: {
    justifyContent: 'center',
    alignItems: 'center',


  },
  rightSectionContainer: {
    flexDirection: 'row',
    flex: 1,
    alignItems: 'center',
    paddingHorizontal: 20,

  },
  mainTitleContainer: {
    justifyContent: 'center',
    flexDirection: 'column',
    flex: 1,
  },
  nameStyle: {
    fontSize: 18,
    color: 'black',
  },
  titleStyle: {
    fontSize: 14,
  },
  phoneContainer: {
    alignItems: 'center',
    paddingHorizontal: 15
  },
  imageContainer: {
    height: 100,
    width: 100,
    overflow: 'hidden',
    borderRadius: Math.round(50 + 50) / 2,
  },
  imageAvatar: {
    height: 130,
    width: 100,
    borderRadius: Math.round(50 + 50) / 2,

  },
  contactContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 5,
  },
  shareContainer: {
    paddingVertical: 5
  }
});
export default ContactDetail;

