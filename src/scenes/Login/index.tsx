import React, {useState, useEffect,createContext} from 'react';
import {
    Alert,
    SafeAreaView,
    StyleSheet,
    Text,
    View,
  } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { TextInput } from 'react-native-paper';
import fn_Login from '../../services/api/login'
import { useSelector, useDispatch } from 'react-redux';
import { setUserInfo, setToken, setContact } from '@services/redux/actions';
import { useNavigation } from '@react-navigation/native';
import { DRAWER } from '@config/constrains';
import { Loader } from '@components/atoms';
import fn_getContact from '@services/api/getContact';
import Storage from 'react-native-storage';
import storage from '@config/initStorage';
import { login } from '@services/function/login';


const Login = () => {
    const [username, setUsername] = useState('');
    const [pass, setPass] = useState('');    
    const loading = useSelector((state: any) => state?.control.isLoading);

    const navigation = useNavigation();
    const loginPress =()=>{
        login(username,pass)
        .then((res:any)=>{
            if(res.status==200){            
                navigation.navigate(DRAWER)
            }       
        }           
        ).catch(()=>{
            Alert.alert("Error","Lỗi đăng nhập");
        });     
    }

    return (
        <SafeAreaView style={styles.container}>
            <Loader loading={loading}/>
            <TextInput
            style={styles.inputStyle}
                label="username"
                value={username}
                onChangeText={username => setUsername(username)}
            />
            <TextInput
                style={styles.inputStyle}
                label="password"
                value={pass}
                secureTextEntry={true}
                onChangeText={pass => setPass(pass)}
            />
            <TouchableOpacity style={{height:50, width:300, alignItems:'center',backgroundColor:'gray'}} 
                onPress={loginPress}>
                <Text>Sign In</Text>
            </TouchableOpacity>
        </SafeAreaView>
      
    );
  };

  const styles= StyleSheet.create({
      container:{
        flex:1,
        alignItems:'center',
        justifyContent:'center',
      },
      inputStyle:{
        height:50,
        width:300,

      }
  })
  
  export default Login;