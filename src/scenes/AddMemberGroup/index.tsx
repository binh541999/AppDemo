import React, {useState, useEffect} from 'react';
import {
    SafeAreaView,
    StyleSheet,
    View,
    FlatList,
    Alert,
    Linking,
    Text
  } from 'react-native';
import { Searchbar } from 'react-native-paper';
import { CustomChooseMember } from '@components/atoms';
import metric from '@config/metrics';
import { useSelector, useDispatch } from 'react-redux';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';



const Contact = () => {
    const [listShow,setListShow] = useState<any[]>([]);
    const renderItem = ({ item }) => (
      <TouchableWithoutFeedback >
           <CustomChooseMember  shortName={item.shortName} phoneNumber={item.phoneNumber}
                          titleName={item.titleName} employeePicUrl={item.employeePicUrl} employeeId={item.employeeId}/>
      </TouchableWithoutFeedback>
       
    );
  
    const [searchQuery, setSearchQuery] = useState('');
    
    const contactList = useSelector((state:any)=> state?.contactList)
    const search =(text:string)=>{
      setListShow(contactList.filter((val:any)=>{
        if(val.shortName != '' && val.shortName.toLowerCase().search(text.toLocaleLowerCase())>-1)
          return true;
        return false;
      }))
      if(text == '')
      setListShow(contactList);
    }

    useEffect (()=>{
     if(contactList != undefined) {   
       setListShow(contactList);
     }     
    },[contactList]);

    return (
        <SafeAreaView>
            <View style={styles.container}>               
                <Searchbar 
                    style={styles.searchBar}
                    placeholder="name, title, phone number"
                    onChangeText={(value)=>{
                      search(value);
                      setSearchQuery(value)

                    }}
                    value={searchQuery}
                    />
                 <FlatList  
                    data={listShow}
                    renderItem={renderItem}
                    horizontal={false}
                    keyExtractor={(item:any)=>
                      item.employeeCode}/> 
                    
                   
            </View>
            
        </SafeAreaView>
        
    )
}

const styles = StyleSheet.create({
    container: {
       height:metric.DEVICE_HEIGHT,
    },
    header: {
      backgroundColor: '#4591ed',
      color: 'white',
      paddingHorizontal: 15,
      paddingVertical: 15,
      fontSize: 20,
    },
    searchBar: {      
      marginHorizontal:15,
      marginVertical:5,
      fontSize:12
    },
    sectionHeaderContainer: {
      height: 30,
      backgroundColor: 'gray',
      justifyContent: 'center',
      paddingHorizontal: 15,
    },
  
    sectionHeaderLabel: {
      color: 'white',
    },
  });

export default Contact;