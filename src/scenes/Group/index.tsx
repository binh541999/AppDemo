import React, { useEffect, useState } from 'react';
import { View, Text, StyleSheet, FlatList, Image, TouchableOpacity } from 'react-native';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { useSelector, useDispatch } from 'react-redux';
import { SafeAreaView } from 'react-native-safe-area-context';
import { IconAddMember, IconMember, IconMessage, IconPlus } from '@assets/svg';
import { CustomContact, CustomCreateGroup } from '@components/atoms';
import metric from '@config/metrics';
import color from '@config/colors';
import { ImageAvatarDefault } from '@assets/images'
import { useNavigation } from '@react-navigation/native';
import { ADDMEMBER, DETAIL } from '@config/constrains';
import { dispatch } from '@navigations';
import { setCurrentGroup } from '@services/redux/actions';
import email from 'react-native-email'


const Group = () => {
    const navigation = useNavigation();
    const renderItem = ({ item }) => (
        <TouchableWithoutFeedback onPress={()=>{
            console.log("🚀 ~ file: index.tsx ~ line 29 ~ Group ~ item", item)
            navigation.navigate(DETAIL,{item:item});
        }}>
             <CustomContact  shortName={item.shortName} phoneNumber={item.phoneNumber}
                            titleName={item.titleName} employeePicUrl={item.employeePicUrl}/>
        </TouchableWithoutFeedback>
         
      );
    const iconStyle = {
    
        width: 25,
        height: 25,
        color: color.WHITE,
        fill: color.WHITE,
    }

    const [modalVisible, setModalVisible] = useState(false);
    const [groupList, setGroupList] = useState<any>([]);
    const [memberList, setMemberList] = useState<any>([]);

    const group = useSelector((state: any) => state?.group);
    const [disabled, setDisable] = useState(false);
    const [refresh, setRefresh] = useState(false);
    const groupMember = useSelector((state: any) => state?.groupMember)   
    const contactList = useSelector((state: any) => state?.contactList)
    const currentIDGroup = useSelector((state: any) => state?.currentIDGroup)

    const getCurrentMemberList = (curentGroup) => {
        //console.log("🚀 ~ file: index.tsx ~ line 39 ~ getCurrentMemberList ~ curentGroup", curentGroup)

        setMemberList(contactList.filter((obj: any) => {
            //console.log("🚀 ~ file: index.tsx ~ line 41 ~ getCurrentMemberList ~ obj", obj)
            let index = groupMember.findIndex((item: any) => {
                //console.log("🚀 ~ file: index.tsx ~ line 41 ~ getCurrentMemberList ~ obj", obj)
                return item.idGroup == curentGroup && item.idMember == obj.employeeId
                //&& groupMember.idGroup == currentIDGroup.id        
            })
            return index > -1 ? true : false
        })
        )

        //console.log("🚀 ~ file: index.tsx ~ line 49 ~ index ~ index", testlist)
        //console.log("🚀 ~ file: index.tsx ~ line 35 ~ Group ~ groupMember", groupMember)
        // console.log("🚀 ~ file: index.tsx ~ line 36 ~ Group ~ currentIDGroup", currentIDGroup)
        //console.log("🚀 ~ file: index.tsx ~ line 30 ~ Group ~ memberList", memberList)
    }
    useEffect(()=>{
        if (group.length != 0 ) {
        console.log("🚀 ~ file: index.tsx ~ line 70 ~ useEffect ~ group", group)
        setGroupList(group);  
            dispatch(setCurrentGroup(group[0]))
            setDisable(false);
        }
    },[group])

    useEffect(() => {
        if (group.length != 0 ) {
            console.log("🚀 ~ file: index.tsx ~ line 73 ~ useEffect ~ currentIDGroup", currentIDGroup)
            console.log("🚀 ~ file: index.tsx ~ line 83 ~ useEffect ~ group", group)
            if( currentIDGroup == group[0].id) {
                
                setDisable(false);
                setGroupList(group);
            //dispatch(setCurrentGroup(group[0]))
                getCurrentMemberList(group[0].id);
            }
            else {
                setDisable(false);
                setGroupList(group);               
                getCurrentMemberList(currentIDGroup.id);
                
            }
            
        }
        else {
            setDisable(true);
        }

    }, [groupMember])

    return (
        <View>
            <View >
                <CustomCreateGroup visible={modalVisible} setVisible={setModalVisible} />
                <View style={{ flexDirection: 'row', marginTop: 5 }}>
                    <FlatList
                        data={groupList}
                        //extraData={refresh}
                        keyExtractor = { (item, index) => {
                            //console.log("🚀 ~ file: index.tsx ~ line 100 ~ Group ~ item", item)
                           return (item.id.toString())} 
                        
                        }
                        // item.item.id.toString()}}
                        renderItem={({ item, index }) => {

                            return (<View style={ item.id == currentIDGroup.id && item.id != null ? 
                                { alignItems: 'center', backgroundColor:color.BLUE }
                            :
                            { alignItems: 'center', backgroundColor:color.WHITE }
                            }>
                               
                                <TouchableOpacity style={styles.groupAvatarStyle}
                                    onPress={() => {
                                        //setCurrentGroup(group[item.id])
                                         //console.log("🚀 ~ file: index.tsx ~ line 70 ~ Group ~ item", item)
                                        //console.log("🚀 ~ file: index.tsx ~ line 55 ~ Group ~ group[item.id]", group[item.id - 1])
                                        dispatch(setCurrentGroup(group[item.id - 1]))
                                        //console.log("🚀 ~ file: index.tsx ~ line 78 ~ useEffect ~ currentIDGroup", currentIDGroup)
                                        getCurrentMemberList(item.id);
                                        //console.log("🚀 ~ file: index.tsx ~ line 55 ~ Group ~ group[item.id]", group)
                                    }}>
                                    <View style={{ flexDirection: 'row', overflow: 'hidden' }}>
                                        <Image source={ImageAvatarDefault} style={{ height: 60, width: 40, marginLeft: -10 }} />
                                        <View style={{ overflow: 'hidden' }}>
                                            <Image source={ImageAvatarDefault} style={{ height: 30, width: 40, marginLeft: -10 }} />
                                            <Image source={ImageAvatarDefault} style={{ height: 30, width: 40, marginLeft: -10 }} />
                                        </View>
                                    </View>
                                </TouchableOpacity>
                                <Text style = {
                                    item.id == currentIDGroup.id ? {color:'white'} : {color:'black'}
                                }>{item.name}</Text>
                            </View>)
                        }


                        }
                        horizontal={true} />
                    <View style={{}}>
                        <TouchableOpacity style={styles.groupAvatarStyle}
                            onPress={() => {
                                setModalVisible(true);
                                //setRefresh(!refresh)
                            }}>
                            <IconPlus iconStyle={iconStyle} />
                        </TouchableOpacity>
                        <Text>New Group</Text>
                    </View>

                </View>

            </View>
            <View style={styles.buttonContainer}>
                <TouchableOpacity style={styles.buttonStyle} disabled={disabled} onPress ={()=>{

                }} >
                    <IconMember iconStyle={iconStyle} />
                </TouchableOpacity>
                <TouchableOpacity style={styles.buttonStyle} disabled={disabled} onPress = {()=>{
                    const emails=memberList.map( (obj)=>{
                        return obj.email
                    });
                    //console.log("🚀 ~ file: index.tsx ~ line 154 ~ emails", emails)

                    email(emails).catch(console.error)
                    
                }}>
                    <IconMessage iconStyle={iconStyle} />
                </TouchableOpacity>
                <TouchableOpacity style={styles.buttonStyle} disabled={disabled} onPress={() => {

                    navigation.navigate(ADDMEMBER);

                }}>
                    <IconAddMember iconStyle={iconStyle} />
                </TouchableOpacity>
            </View>
            <View >
                <FlatList
                    data={memberList}
                    keyExtractor = { (item, index) => {
                        //console.log("🚀 ~ file: index.tsx ~ line 171 ~ item", item)
                       return(item.employeeId.toString())  }}
                    renderItem={renderItem}
                />

            </View>
        </View>
    )
}
const styles = StyleSheet.create({
    buttonContainer: {
                    
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-evenly',
        backgroundColor: color.BLUE,
        height: metric.DEVICE_HEIGHT * 0.06,
        width: metric.DEVICE_WIDTH
    },
    buttonStyle: {
        width: metric.DEVICE_WIDTH / 3,
        alignItems: 'center'

    },

    groupAvatarStyle: {
        borderRadius: 50,
        height: 60,
        width: 60,
        backgroundColor: 'white',
        marginHorizontal: 10,
        alignItems: 'center', justifyContent: 'center',
        overflow: 'hidden'

    }
})

export default Group;